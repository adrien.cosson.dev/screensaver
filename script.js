const canvas = document.getElementById("app")
const ctx = canvas.getContext("2d")

var cRect = canvas.getBoundingClientRect()

document.body.style.margin = "0"

var screenWidth = innerWidth - cRect.left
var screenHeight = innerHeight - cRect.top

var form = []
var position = []
var directions = []
var formsDirections = []
var a = 0

let firstEdge = random(screenWidth)
let secondEdge = random(screenWidth)
let thirdEdge = random(screenWidth)
let fourthEdge = random(screenWidth)

let firstEdgeSpeed = 0
let secondEdgeSpeed = 0
let thirdEdgeSpeed = 0
let fourthEdgeSpeed = 0
var formPosition = [random(30), random(30), random(30), random(30),]
// var speed = [firstEdgeSpeed, secondEdgeSpeed, thirdEdgeSpeed, fourthEdgeSpeed]
var speed = [0,0,0,0]

function createForm() {
    for (let i = 0; i < 10; i++) {
        console.log("speed "+speed);
        position = [
            [firstEdge + speed[0], firstEdge + speed[0]],
            [secondEdge + speed[1], thirdEdge - speed[1]],
            [thirdEdge - speed[2], thirdEdge - speed[2]],
            [fourthEdge - speed[3], fourthEdge + speed[3]],
            [firstEdge + speed[0], firstEdge + speed[0]],
        ]
        directions = [
            [false, true],
            [true, true],
            [true, true],
            [false, false],
        ]
        // directions = [
        //     [Math.random() < 0.5, Math.random() < 0.5],
        //     [Math.random() < 0.5, Math.random() < 0.5],
        //     [Math.random() < 0.5, Math.random() < 0.5],
        //     [Math.random() < 0.5, Math.random() < 0.5],
        // ]
        if (i <= speed.length) {
            for (let j = 0; j < speed.length; j++) {
                speed[j] += formPosition[j]
            }


        }
        form.push(position)
        formsDirections.push(directions)

    }
    drawRect()
}

console.log(directions);

canvas.width = screenWidth
canvas.height = screenHeight

function refreshCanvas() {

    ctx.clearRect(0,0, screenWidth, screenHeight)
    ctx.fillStyle = "white"
    drawRect()
}

function drawRect() {

    for (let j = 0; j < form.length; j++) {
        ctx.beginPath()
        ctx.moveTo(form[j][0][0], form[j][0][1])
        for (let i = 1; i <= position.length-1; i++) {
            ctx.lineTo(form[j][i][0], form[j][i][1])
        }
        ctx.stroke()
    }
    setTimeout(moveRect, 80)
}

function moveRect() {

    console.log(form.length);
    for (let j = 0; j < form.length; j++) {
        for (let i = 0; i < form[j].length - 1; i++) {
            moveEdge(j, i)
        }
    }
    refreshCanvas()
}

function moveEdge(formNumber, edgeNumber) {

        if (formsDirections[formNumber][edgeNumber][0] === true && formsDirections[formNumber][edgeNumber][1] === true) {
            form[formNumber][edgeNumber][0] += formPosition[edgeNumber]
            form[formNumber][edgeNumber][1] += formPosition[edgeNumber]
            if (edgeNumber === 0) {
                form[formNumber][4][0] += formPosition[edgeNumber]
                form[formNumber][4][1] += formPosition[edgeNumber]
            }
        }
        if (formsDirections[formNumber][edgeNumber][0] === true && formsDirections[formNumber][edgeNumber][1] !== true) {
            form[formNumber][edgeNumber][0] += formPosition[edgeNumber]
            form[formNumber][edgeNumber][1] -= formPosition[edgeNumber]
            if (edgeNumber === 0) {
                form[formNumber][4][0] += formPosition[edgeNumber]
                form[formNumber][4][1] -= formPosition[edgeNumber]
            }
        }
        if (formsDirections[formNumber][edgeNumber][0] !== true && formsDirections[formNumber][edgeNumber][1] === true) {
                form[formNumber][edgeNumber][0] -= formPosition[edgeNumber]
                form[formNumber][edgeNumber][1] += formPosition[edgeNumber]
                if (edgeNumber === 0) {
                    form[formNumber][4][0] -= formPosition[edgeNumber]
                    form[formNumber][4][1] += formPosition[edgeNumber]
                }
        }
        if (formsDirections[formNumber][edgeNumber][0] !== true && formsDirections[formNumber][edgeNumber][1] !== true) {
            form[formNumber][edgeNumber][0] -= formPosition[edgeNumber]
            form[formNumber][edgeNumber][1] -= formPosition[edgeNumber]
            if (edgeNumber === 0) {
                form[formNumber][4][0] -= formPosition[edgeNumber]
                form[formNumber][4][1] -= formPosition[edgeNumber]
            }
        }
    changeDirections(formNumber, edgeNumber)
}

function changeDirections(formNumber, edgeNumber) {

    if (form[formNumber][edgeNumber][0] >= screenWidth) {
        formsDirections[formNumber][edgeNumber][0] = false
        // speed[1] = random()
    }
    else if (form[formNumber][edgeNumber][0] <= 0) {
        formsDirections[formNumber][edgeNumber][0] = true
    }
    if (form[formNumber][edgeNumber][1] >= screenHeight) {

        formsDirections[formNumber][edgeNumber][1] = false
    } else if (form[formNumber][edgeNumber][1] <= 0) {
        formsDirections[formNumber][edgeNumber][1] = true
        if (formNumber === form.length-1) {
            console.log("formNumber: "+formNumber);
            // speed[0] = random(10)
            // console.log("random "+speed[0]);
        }
    }
}

function random(max) {
    return Math.floor(Math.random() * max) + 1
}

createForm()
